[See my draft](https://gitlab.com/arttonoyan/MapStruct/blob/master/MapStructTestApp/MapStructTestApp/Program.cs)

### Specific Configuration
```
Mapper.MapConfiguration(cfg =>
    {
        cfg.CreateMap<MyClass, MyClassDto>()
            .Property(vm => vm.MyClass1Dto, m => m
                .AsMapable()
                .With<MyClass1Dto>()
                .Convert());
    });

```

### MapConvert
```
IEnumerable<MyClass> arr = Enumerable.Empty<MyClass>();
IEnumerable<MyClassDto> arr1 = arr.MapConvert().To<MyClassDto>();
```

### Create Func for convert
#### Example 1
```
var func = Mapable.AsMapable<MyClass>().With<MyClassDto>().ToFunc();
```
#### Example 2
```
MyClassDto myDto = my
    .AsMapable()
    .With<MyClassDto>()
    .Where(cfg => cfg
        .Property(dto => dto.Age, m => m.Age ?? default(int))
        .Ignore(dto => dto.MyClass1Dto))
    .Convert();
```
