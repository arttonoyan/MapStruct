﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using MapStruct;

namespace MapStructDataReaderTestApp
{
    public interface IDataRepository
    {
        IEnumerable<TModel> Execute<TModel>(string query) where TModel : class, new();
        //IEnumerable<TModel> Execute<TModel>(string query, Func<IDataRecord, TModel> convertFunc) where TModel : class, new();
    }

    class DataRepository : IDataRepository
    {
        public DataRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        private readonly string _connectionString;

        public IEnumerable<TModel> Execute<TModel>(string query)
            where TModel : class, new()
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                if(conn.State != ConnectionState.Open)
                    conn.Open();

                using (var comand = conn.CreateCommand())
                {
                    comand.CommandType = CommandType.Text;
                    comand.CommandText = query;
                    var reader = comand.ExecuteReader();
                    var mapConverter = Mapper.Converter<TModel>(reader);
                    while (reader.Read())
                        yield return mapConverter.Invoke(reader);
                }
            }
        }
    }
}