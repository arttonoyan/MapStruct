﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using MapStruct;
using MapStruct.Linq;

namespace MapStructDataReaderTestApp
{
    static class Test
    {
        // This is the method you want, I think
        public static Expression<Func<TInput, object>> AddBox<TInput, TOutput>(this Expression<Func<TInput, TOutput>> expression)
        {
            // Add the boxing operation, but get a weakly typed expression
            Expression converted = Expression.Convert(expression.Body, typeof(object));
            // Use Expression.Lambda to get back to strong typing
            return Expression.Lambda<Func<TInput, object>>(converted, expression.Parameters);
        }

        public static Expression<Func<TInput, TOutput>> AddBox<TInput, TOutput>(this Expression<Func<TInput, object>> expression)
        {
            // Add the boxing operation, but get a weakly typed expression
            Expression converted = Expression.Convert(expression.Body, typeof(TOutput));
            // Use Expression.Lambda to get back to strong typing
            return Expression.Lambda<Func<TInput, TOutput>>(converted, expression.Parameters);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Expression<Func<System.Data.IDataRecord, object>> exp = r => new Code();
            //Expression<Func<System.Data.IDataRecord, Code>> exp1 = exp.AddBox<System.Data.IDataRecord, Code>();

            MapperConfig();
            IDataRepository repository = new DataRepository(ConnectionString2);
            List<Section> sections = repository.Execute<Section>("SELECT * FROM [dbo].[SectionView]").ToList();
            //List<Code> codes = repository.Execute<Code>("SELECT * FROM [dbo].[code]").ToList();
        }

        static void MapperConfig()
        {
            Mapper.MapConfiguration(cfg =>
            {
                //cfg.CreateDataReaderMap<Section>().UseStandardCodeStyleForMembers()
                //    .Property(model => model.Code, p => p
                //        .AsMapable()
                //        .With<Code>()
                //        .Where(m => m
                //            .Property(wm => wm.Name, "CodeName")
                //            .Property(wm => wm.OrderIndex, "CodeOrderIndex")
                //            .Property(wm => wm.Id, "CodeId")
                //        )
                //        .Convert());

                cfg.CreateDataReaderMap<Section>()
                    //.UseStandardCodeStyleForMembers()
                    .Property(model => model.Code, p => p
                        .AsDapable<Code>()
                        .Where(m => m
                            .Property(wm => wm.Name, "CodeName".ToUpper())
                            .Property(wm => wm.OrderIndex, "CodeOrderIndex".ToUpper())
                            .Property(wm => wm.Id, "CodeId".ToUpper()))
                    );

                //cfg.CreateDataReaderMap<Code>().UseStandardCodeStyleForMembers();
            });
        }

        private const string ConnectionString =
            @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        private const string ConnectionString1 =
            @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=ELawyerDB;Integrated Security=True";

        private const string ConnectionString2 =
            @"Data Source=ARTYOMT-280;Initial Catalog=ELawyer;Integrated Security=True";


        class Section
        {
            public long Id { get; set; }
            public string Name { get; set; }
            public int OrderIndex { get; set; }
            public int CodeId { get; set; }

            public Code Code { get; set; }

            public override string ToString()
            {
                return Name;
            }
        }

        class Code
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int OrderIndex { get; set; }

            public override string ToString()
            {
                return Name;
            }
        }

        class Section1
        {
            public object GetFileld() => 10;
            public int SectionId { get; set; }
            public string SectionName { get; set; }
            public int SectionOrder { get; set; }
            public int CodeId { get; set; }
        }
    }
}