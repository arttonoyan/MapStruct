﻿using System.Collections.Generic;
using System.Linq;
using MapStruct.Linq.Builders;

namespace MapStruct.Linq
{
    public interface IMapable<TSource> where TSource : class
    {
        IMapBuilder<TSource, TDestination> With<TDestination>()
            where TDestination : class, new();

        //IMapBuilder<TSource, IDataRecord> WithDataReader();

        TDestination Convert<TDestination>()
            where TDestination : class, new();
    }

    public interface IDapable<TDestination>
        where TDestination : class, new()
    {
        IDapBuilder<TDestination> With();

        //IMapBuilder<TSource, IDataRecord> WithDataReader();

        TDestination Convert();
    }

    public interface IMapEnumerable<TSource> where TSource : class
    {
        IEnumerable<TDestination> To<TDestination>()
            where TDestination : class, new();
    }

    internal class MapEnumerable<TSource> : IMapEnumerable<TSource> 
        where TSource : class
    {
        public MapEnumerable(IEnumerable<TSource> sources)
        {
            _sources = sources;
        }

        private IEnumerable<TSource> _sources;

        public IEnumerable<TDestination> To<TDestination>() 
            where TDestination : class, new()
        {
            var Convert = Mapper.Converter<TSource, TDestination>();
            return _sources.Select(Convert);
        }
    }
}
