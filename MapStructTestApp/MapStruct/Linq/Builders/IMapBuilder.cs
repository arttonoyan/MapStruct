﻿using System;
using System.Data;
using MapStruct.Builders;

namespace MapStruct.Linq.Builders
{
    public interface IMapBuilder
    {
        void Execute();
    }

    public interface IMapBuilder<TSource, TDestination> : IMapBuilder
        where TSource : class
        where TDestination : class, new()
    {
        IMapBuilder<TSource, TDestination> Where(Action<IModelTypeConfigurationBuilder<TSource, TDestination>> builderAction);

        Func<TSource, TDestination> ToFunc();
        Func<TSource, TDestination> ToFuncAndCashe();
        TDestination Convert();
    }

    public interface IDapBuilder1<out TDestination> : IMapBuilder
    {
        Func<IDataRecord, TDestination> ToFunc();
        Func<IDataRecord, TDestination> ToFuncAndCashe();
        TDestination Convert();
    }

    public interface IDapBuilder<TDestination> : IDapBuilder1<TDestination>
        where TDestination : class, new()
    {
        IDapBuilder<TDestination> Where(Action<IDataReaderConfigurationBuilder<TDestination>> builderAction);
    }
}