﻿using MapStruct.Builders;
using System;
using MapStruct.Builders.Implementations;
using System.Collections.Generic;
using System.Data;
using MapStruct.Providers;

namespace MapStruct.Linq.Builders.Implementations
{
    internal class MapBuilder<TSource, TDestination> : IMapBuilder<TSource, TDestination>
        where TSource : class
        where TDestination : class, new()
    {
        public MapBuilder(TSource source, IModelTypeConfigurationBuilder<TSource, TDestination> configurationBuilder)
        {
            _source = source;
            _configurationBuilder = configurationBuilder;
        }

        public MapBuilder(IEnumerable<TSource> source1, IModelTypeConfigurationBuilder<TSource, TDestination> configurationBuilder)
        {
            _source1 = source1;
            _configurationBuilder = configurationBuilder;
        }

        private readonly TSource _source;
        private IEnumerable<TSource> _source1;
        private readonly IModelTypeConfigurationBuilder<TSource, TDestination> _configurationBuilder;
        
        public IMapBuilder<TSource, TDestination> Where(Action<IModelTypeConfigurationBuilder<TSource, TDestination>> builderAction)
        {
            builderAction.Invoke(_configurationBuilder);
            return this;
        }

        public void Execute()
        {
            ((ModelTypeConfigurationBuilder<TSource, TDestination>) _configurationBuilder).Finish();
        }

        public Func<TSource, TDestination> ToFunc()
        {
            return ToFuncAndCashe();
            //var settins = ((ConfigurationBuilder) _configurationBuilder).Settings;
            //return DataReaderMapProvider.CreateExpression<TDestination>(_source, settins).Compile();
        }

        public TDestination Convert()
        {
            var converter = ToFunc();
            return converter.Invoke(_source);
        }

        public Func<TSource, TDestination> ToFuncAndCashe()
        {
            Execute();
            return Mapper.Converter<TSource, TDestination>();
        }
    }

    internal class DapBuilder<TDestination> : IDapBuilder<TDestination>
        where TDestination : class, new()
    {
        public DapBuilder(IDataRecord source, IDataReaderConfigurationBuilder<TDestination> configurationBuilder)
        {
            _source = source;
            _configurationBuilder = configurationBuilder;
        }

        private readonly IDataRecord _source;
        private readonly IDataReaderConfigurationBuilder<TDestination> _configurationBuilder;
        
        public IDapBuilder<TDestination> Where(Action<IDataReaderConfigurationBuilder<TDestination>> builderAction)
        {
            builderAction.Invoke(_configurationBuilder);
            return this;
        }

        public void Execute()
        {
            ((DataReaderConfigurationBuilder<TDestination>) _configurationBuilder).Finish();
        }

        public Func<IDataRecord, TDestination> ToFuncAndCashe()
        {
            Execute();
            return Mapper.Converter<TDestination>(_source);
        }

        public Func<IDataRecord, TDestination> ToFunc()
        {
            //var settins = ((DataReaderConfigurationBuilder<TDestination>) _configurationBuilder).Settings;
            var settins = ((ConfigurationBuilder) _configurationBuilder).Settings;
            return DataReaderMapProvider.CreateExpression<TDestination>(_source, settins).Compile();
        }

        public TDestination Convert()
        {
            var converter = ToFunc();
            return converter.Invoke(_source);
        }
    }
}