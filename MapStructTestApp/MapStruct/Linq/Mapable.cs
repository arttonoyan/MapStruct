﻿using System.Collections.Generic;
using System.Data;
using MapStruct.Linq.Implementations;

namespace MapStruct.Linq
{
    public static class Mapable
    {
        public static IMapable<TSource> AsMapable<TSource>() where TSource : class
        {
            return new MapableModel<TSource>(default(TSource));
        }

        public static IMapable<TSource> AsMapable<TSource>(this TSource source) where TSource : class
        {
            return new MapableModel<TSource>(source);
        }

        public static IMapable<TSource> AsMapable<TSource>(this IEnumerable<TSource> source) where TSource : class
        {
            return new MapableEnumerable<TSource>(source);
        }

        //public static IDapable<TDestination> AsDapable<TDestination>(this IDataRecord source)
        //    where TDestination : class, new()
        //{
        //    return new DapableModel<TDestination>(source);
        //}

        public static Builders.IDapBuilder<TDestination> AsDapable<TDestination>(this IDataRecord source)
            where TDestination : class, new()
        {
            return new DapableModel<TDestination>(source).With();
        }
    }
}