﻿using System;
using System.Collections.Generic;
using System.Data;
using MapStruct.Builders;
using MapStruct.Builders.Implementations;
using MapStruct.Linq.Builders;
using MapStruct.Linq.Builders.Implementations;

namespace MapStruct.Linq.Implementations
{
    internal class DapableModel<TDestination> : IDapable<TDestination>
        where TDestination : class, new()
    {
        public DapableModel(IDataRecord source)
        {
            _source = source;
            _modelTypeBuilder = new ModelTypeBuilder();
        }

        private readonly IDataRecord _source;
        private readonly ModelTypeBuilder _modelTypeBuilder;

        public IDapBuilder<TDestination> With()
        {
            var builder = _modelTypeBuilder.CreateDataReaderMap<TDestination>();
            //var mapBuilder = new MapBuilder<TSource, TDestination>(_source, builder);
            return new DapBuilder<TDestination>(_source, builder);
        }

        public TDestination Convert()
        {
            return Mapper.Converter<IDataRecord, TDestination>().Invoke(_source);
        }
    }

    //internal class DapableModel : IMapable<IDataRecord>
    //{
    //    protected DapableModel(IDataRecord source)
    //    {
    //        _source = source;
    //        _modelTypeBuilder = new ModelTypeBuilder();
    //    }

    //    private readonly IDataRecord _source;
    //    private readonly ModelTypeBuilder _modelTypeBuilder;

    //    public IMapBuilder<IDataRecord, TDestination> With<TDestination>() where TDestination : class, new()
    //    {
    //        var builder = _modelTypeBuilder.CreateDataReaderMap<TDestination>();
    //        return new MapBuilder<IDataRecord, TDestination>(_source, builder);
    //    }

    //    public TDestination Convert<TDestination>() where TDestination : class, new()
    //    {
    //        return Mapper.Converter<IDataRecord, TDestination>().Invoke(_source);
    //    }
    //}

    internal abstract class BaseMapable<TSource> : IMapable<TSource> where TSource : class
    {
        protected BaseMapable()
        {
            _modelTypeBuilder = new ModelTypeBuilder();
        }

        private readonly ModelTypeBuilder _modelTypeBuilder;

        public IMapBuilder<TSource, TDestination> With<TDestination>()
            where TDestination : class, new()
        {
            var builder = _modelTypeBuilder.CreateMap<TSource, TDestination>();
            //var mapBuilder = new MapBuilder<TSource, TDestination>(_source, builder);
            var mapBuilder = NewMapBuilder(builder);
            return mapBuilder;
        }

        protected abstract IMapBuilder<TSource, TDestination> NewMapBuilder<TDestination>(IModelTypeConfigurationBuilder<TSource, TDestination> builder) 
            where TDestination : class, new();

        public TDestination Convert<TDestination>() 
            where TDestination : class, new()
        {
            return OnConvert<TDestination>();
        }

        protected abstract TDestination OnConvert<TDestination>() where TDestination : class, new();
    }

    internal class MapableModel<TSource> : BaseMapable<TSource> 
        where TSource : class
    {
        public MapableModel(TSource source)
        {
            _source = source;
        }

        private readonly TSource _source;

        protected override IMapBuilder<TSource, TDestination> NewMapBuilder<TDestination>(IModelTypeConfigurationBuilder<TSource, TDestination> builder)
        {
            return new MapBuilder<TSource, TDestination>(_source, builder);
        }

        protected override TDestination OnConvert<TDestination>()
        {
            return Mapper.Converter<TSource, TDestination>().Invoke(_source);
        }
    }

    internal class MapableEnumerable<TSource> : BaseMapable<TSource>/*, IEnumerable<TSource>*/ where TSource : class
    {
        public MapableEnumerable(IEnumerable<TSource> source)
        {
            _source = source;
        }

        private readonly IEnumerable<TSource> _source;

        protected override IMapBuilder<TSource, TDestination> NewMapBuilder<TDestination>(IModelTypeConfigurationBuilder<TSource, TDestination> builder)
        {
            return new MapBuilder<TSource, TDestination>(_source, builder);
        }

        protected override TDestination OnConvert<TDestination>()
        {
            throw new System.NotImplementedException();
        }
    }
}