﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;

namespace MapStruct.Builders
{
    public interface IModelTypeConfigurationBuilder<TSource, TDestination>
        where TSource : class
        where TDestination : class, new()
    {
        IModelTypeConfigurationBuilder<TSource, TDestination> Property<TProperty>(Expression<Func<TDestination, TProperty>> destProperty, Expression<Func<TSource, TProperty>> sourceProperty);
        IModelTypeConfigurationBuilder<TSource, TDestination> Property<TProperty>(Expression<Func<TDestination, TProperty>> destProperty, string name);
        IModelTypeConfigurationBuilder<TSource, TDestination> StaticProperty<TProperty>(Expression<Func<TDestination, TProperty>> destProperty, TProperty sourceProperty);
        IPropertyConfigurationBuilder<TPropertyModel, TDestination> IfIsNotNull<TPropertyModel>(Expression<Func<TSource, TPropertyModel>> sourceProperty) where TPropertyModel : class;
        IModelTypeConfigurationBuilder<TSource, TDestination> Ignore(IEnumerable<string> members);
        IModelTypeConfigurationBuilder<TSource, TDestination> Ignore(params string[] members);
        IModelTypeConfigurationBuilder<TSource, TDestination> Ignore(Expression<Func<TDestination, object>> destProperty);
        IModelTypeConfigurationBuilder<TSource, TDestination> Property<TProperty>(Expression<Func<TSource, TProperty>> sourceProperty, string columnName);
    }
}