﻿namespace MapStruct.Builders
{
    public interface IModelTypeBuilder
    {
        IModelTypeConfigurationBuilder<TSource, TDestination> CreateMap<TSource, TDestination>()
            where TSource : class
            where TDestination : class, new();

        IModelTypeConfigurationBuilder<TSource, TDestination> CreateMergeMap<TSource, TDestination>()
            where TSource : class
            where TDestination : class, new();

        IDataReaderConfigurationBuilder<TModel> CreateDataReaderMap<TModel>()
            where TModel : class, new();
    }
}