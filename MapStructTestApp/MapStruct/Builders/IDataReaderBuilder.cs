﻿using System.Data;

namespace MapStruct.Builders
{
    public interface IDataReaderBuilder<TModel> : IModelTypeConfigurationBuilder<IDataRecord, TModel>
        where TModel : class, new()
    {
        IDataReaderBuilder<TModel> UseStandardCodeStyleForMembers();
        //IDataReaderBuilder<TModel> Property<TProperty>(Expression<Func<TModel, TProperty>> propertyExp, string columnName);

        //IPropertyConfigurationBuilder<TPropertyModel, TModel> IfIsNotNull<TPropertyModel>(Expression<Func<TModel, TPropertyModel>> modelProperty) 
        //    where TPropertyModel : class;
        //IDataReaderBuilder<TModel> Ignore(IEnumerable<string> members);
        //IDataReaderBuilder<TModel> Ignore(params string[] members);
        //IDataReaderBuilder<TModel> Ignore(Expression<Func<TModel, object>> predicate);
    }
}