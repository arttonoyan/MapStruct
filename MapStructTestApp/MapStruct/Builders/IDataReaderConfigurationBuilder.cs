﻿using System;
using System.Data;
using System.Linq.Expressions;
using MapStruct.Linq.Builders;

namespace MapStruct.Builders
{
    public interface IDataReaderConfigurationBuilder<TDestination> : IModelTypeConfigurationBuilder<IDataRecord, TDestination>
        where TDestination : class, new()
    {
        IDataReaderConfigurationBuilder<TDestination> UseStandardCodeStyleForMembers();
        //IDataReaderBuilder<TModel> Property<TProperty>(Expression<Func<TModel, TProperty>> propertyExp, string columnName);

        //IPropertyConfigurationBuilder<TPropertyModel, TModel> IfIsNotNull<TPropertyModel>(Expression<Func<TModel, TPropertyModel>> modelProperty) 
        //    where TPropertyModel : class;
        //IDataReaderBuilder<TModel> Ignore(IEnumerable<string> members);
        //IDataReaderBuilder<TModel> Ignore(params string[] members);
        //IDataReaderBuilder<TModel> Ignore(Expression<Func<TModel, object>> predicate);

        //Action<IDataReaderConfigurationBuilder<TDestination>> IDapBuilder<TDestination>
        IModelTypeConfigurationBuilder<IDataRecord, TDestination> Property<TProperty>(
            Expression<Func<TDestination, TProperty>> destProperty,
            Expression<Func<IDataRecord, IDapBuilder<TProperty>>> action) where TProperty : class, new();
    }
}