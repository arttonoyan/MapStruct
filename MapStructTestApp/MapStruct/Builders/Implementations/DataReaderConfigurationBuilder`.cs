﻿using System;
using System.Data;
using System.Linq.Expressions;
using MapStruct.Linq.Builders;

namespace MapStruct.Builders.Implementations
{
    internal class DataReaderConfigurationBuilder<TModel> : ModelTypeConfigurationBuilder<IDataRecord, TModel>, IDataReaderConfigurationBuilder<TModel>
        where TModel : class, new()
    {
        public DataReaderConfigurationBuilder(ModelTypeBuilder modelTypeBuilder) : base(modelTypeBuilder) { }

        public IModelTypeConfigurationBuilder<IDataRecord, TModel> Property<TProperty>(Expression<Func<TModel, TProperty>> destProperty, Expression<Func<IDataRecord, IDapBuilder<TProperty>>> action) 
            where TProperty : class, new()
        {
            if (!(destProperty.Body is MemberExpression memberExp1))
                return this;
            
            settings.memberExpressions.Add((memberExp1.Member, action));
            return this;
        }

        //private bool _useStandardCodeStyleForMembers;

        public IDataReaderConfigurationBuilder<TModel> UseStandardCodeStyleForMembers()
        {
            settings.useStandardCodeStyleForMembers = true;
            return this;
        }

        internal override void Finish()
        {
            _modelTypeBuilder.FinishDataReaderMap<TModel>(settings);
        }
    }

    //internal class DataReaderBuilder1<TModel> : ConfigurationBuilder, IDataReaderBuilder<TModel>
    //    where TModel : class, new()
    //{
    //    private readonly ModelTypeBuilder _modelTypeBuilder;
    //    private bool _useStandardCodeStyleForMembers;

    //    public DataReaderBuilder1(ModelTypeBuilder modelTypeBuilder)
    //    {
    //        _modelTypeBuilder = modelTypeBuilder;
    //    }

    //    public IDataReaderBuilder<TModel> UseStandardCodeStyleForMembers()
    //    {
    //        _useStandardCodeStyleForMembers = true;
    //        return this;
    //    }

    //    public IDataReaderBuilder<TModel> Property<TProperty>(Expression<Func<TModel, TProperty>> propertyExp, string columnName)
    //    {
    //        OnProperty(propertyExp, columnName);
    //        return this;
    //    }

    //    public void Finish()
    //    {
    //        _modelTypeBuilder.FinishDataReaderMap<TModel>(_memberNameBindings.IsNullOrEmpty()
    //            ? null
    //            : _memberNameBindings, _ignoreMembers, _useStandardCodeStyleForMembers);

    //        Dispose();
    //    }

    //    public IPropertyConfigurationBuilder<TPropertyModel, TModel> IfIsNotNull<TPropertyModel>(Expression<Func<TModel, TPropertyModel>> modelProperty)
    //        where TPropertyModel : class
    //        => new PropertyConfigurationBuilder<TPropertyModel, TModel>(_modelTypeBuilder);

    //    public IDataReaderBuilder<TModel> Ignore(IEnumerable<string> members)
    //    {
    //        OnIgnore(members);
    //        return this;
    //    }

    //    public IDataReaderBuilder<TModel> Ignore(params string[] members)
    //        => Ignore((IEnumerable<string>)members);

    //    public IDataReaderBuilder<TModel> Ignore(Expression<Func<TModel, object>> predicate)
    //    {
    //        OnIgnore(predicate);
    //        return this;
    //    }
    //}
}