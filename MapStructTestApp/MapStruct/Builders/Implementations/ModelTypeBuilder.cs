﻿using System;
using MapStruct.Cashe;
using MapStruct.Configurations;
using MapStruct.Providers;

namespace MapStruct.Builders.Implementations
{
    internal class ModelTypeBuilder : IModelTypeBuilder
    {
        internal Action ConfigurationFinish;
        private ICreationTimeMappingProvider _provider;

        public IModelTypeConfigurationBuilder<TSource, TDestination> CreateMap<TSource, TDestination>()
            where TSource : class
            where TDestination : class, new()
        {
            _provider = new ModelMapProvider();
            return OnCreateMapBuilder<TSource, TDestination>();
        }

        public IModelTypeConfigurationBuilder<TSource, TDestination> CreateMergeMap<TSource, TDestination>()
            where TSource : class
            where TDestination : class, new()
        {
            _provider = new ModelMergeMapProvider();
            return OnCreateMapBuilder<TSource, TDestination>();
        }

        protected IModelTypeConfigurationBuilder<TSource, TDestination> OnCreateMapBuilder<TSource, TDestination>()
            where TSource : class
            where TDestination : class, new()
        {
            var builder = new ModelTypeConfigurationBuilder<TSource, TDestination>(this);
            ConfigurationFinish += builder.Finish;
            return builder;
        }

        public IDataReaderConfigurationBuilder<TModel> CreateDataReaderMap<TModel>()
            where TModel : class, new()
        {
            var builder = new DataReaderConfigurationBuilder<TModel>(this);
            ConfigurationFinish += builder.Finish;
            return builder;
        }
        
        internal void FinishDataReaderMap<TModel>(ConfigurationSettings configuration)
            where TModel : class, new()
        {
            CasheDataReader<TModel>.SetConfiguration(configuration);
        }

        internal void FinishMap<TSource, TDestination>(ConfigurationSettings configuration)
            where TSource : class
            where TDestination : class, new()
        {
            var exp = _provider.CreateExpression<TSource, TDestination>(configuration);
            _provider.ExecuteAndCashe<TSource, TDestination>(exp);
        }
    }
}