﻿namespace MapStruct.Builders.Implementations
{
    internal class PropertyConfigurationBuilder<TSource, TDestination> : ModelTypeConfigurationBuilder<TSource, TDestination>, IPropertyConfigurationBuilder<TSource, TDestination>
        where TSource : class
        where TDestination : class, new()
    {
        public PropertyConfigurationBuilder(ModelTypeBuilder modelTypeBuilder)
            : base(modelTypeBuilder)
        { }
    }
}