﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MapStruct.Builders.Implementations
{
    internal class ModelTypeConfigurationBuilder<TSource, TDestination> : ConfigurationBuilder, IModelTypeConfigurationBuilder<TSource, TDestination>
        where TSource : class
        where TDestination : class, new()
    {
        public ModelTypeConfigurationBuilder(ModelTypeBuilder modelTypeBuilder)
        {
            _modelTypeBuilder = modelTypeBuilder;
        }

        // The key is model2 member name.
        protected readonly ModelTypeBuilder _modelTypeBuilder;

        public IModelTypeConfigurationBuilder<TSource, TDestination> Property<TProperty>(Expression<Func<TDestination, TProperty>> destProperty, Expression<Func<TSource, TProperty>> sourceProperty)
        {
            OnProperty(destProperty, sourceProperty);
            return this;
        }

        public IModelTypeConfigurationBuilder<TSource, TDestination> Property<TProperty>(Expression<Func<TDestination, TProperty>> destProperty, string name)
        {
            OnProperty(destProperty, name);
            return this;
        }

        public IModelTypeConfigurationBuilder<TSource, TDestination> Property<TProperty>(Expression<Func<TSource, TProperty>> sourceProperty, string columnName)
        {
            OnProperty(sourceProperty, columnName);
            return this;
        }

        public IModelTypeConfigurationBuilder<TSource, TDestination> StaticProperty<TProperty>(Expression<Func<TDestination, TProperty>> destProperty, TProperty sourceProperty)
        {
            OnProperty(destProperty, sourceProperty);
            return this;
        }

        public IModelTypeConfigurationBuilder<TSource, TDestination> Ignore(params string[] members) 
            => Ignore((IEnumerable<string>)members);

        public IModelTypeConfigurationBuilder<TSource, TDestination> Ignore(IEnumerable<string> members)
        {
            OnIgnore(members);
            return this;
        }

        public IModelTypeConfigurationBuilder<TSource, TDestination> Ignore(Expression<Func<TDestination, object>> destProperty)
        {
            OnIgnore(destProperty);
            return this;
        }

        public IPropertyConfigurationBuilder<TPropertyModel, TDestination> IfIsNotNull<TPropertyModel>(Expression<Func<TSource, TPropertyModel>> sourceProperty) 
            where TPropertyModel : class 
            => new PropertyConfigurationBuilder<TPropertyModel, TDestination>(_modelTypeBuilder);

        internal virtual void Finish()
            => _modelTypeBuilder.FinishMap<TSource, TDestination>(settings);
    }
}
