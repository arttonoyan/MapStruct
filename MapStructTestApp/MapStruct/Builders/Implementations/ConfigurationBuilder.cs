﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MapStruct.Extensions;
using MapStruct.Configurations;
using MapStruct.Helpers;

namespace MapStruct.Builders.Implementations
{
    internal class ConfigurationBuilder : IDisposable
    {
        protected ConfigurationBuilder()
        {
            settings = new ConfigurationSettings();
        }

        protected ConfigurationSettings settings;

        public void OnProperty(LambdaExpression model2Property, object value)
        {
            if (!(model2Property.Body is MemberExpression memberExp1))
                return;

            settings.memberBindings.Add(memberExp1.Member.Name, Expression.Bind(memberExp1.Member, Expression.Constant(value)));
        }

        public void OnProperty(LambdaExpression model2Property, LambdaExpression model1Property)
        {
            if (!(model2Property.Body is MemberExpression memberExp1))
                return;

            settings.memberBindings.Add(memberExp1.Member.Name, Expression.Bind(memberExp1.Member, model1Property.Body));
        }

        protected void OnProperty(LambdaExpression propertyExp, string columnName)
        {
            if (!(propertyExp.Body is MemberExpression memberExp1))
                return;

            settings.memberNameBindings[memberExp1.Member.Name] = columnName;
        }

        protected void OnIgnore(IEnumerable<string> members) 
            => settings.ignoreMembers.AddRange(members);

        protected void OnIgnore(LambdaExpression expression)
        {
            string name = expression.GetMemberName();
            if (!string.IsNullOrEmpty(name))
                settings.ignoreMembers.Add(name);
        }

        internal ConfigurationSettings Settings => settings;

        #region IDisposable Support

        private bool _disposedValue;

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    settings?.Dispose();
                }

                _disposedValue = true;
            }
        }
        #endregion
    }
}
