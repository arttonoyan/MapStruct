﻿namespace MapStruct.Builders
{
    public interface IPropertyConfigurationBuilder<TSource, TDestination> : IModelTypeConfigurationBuilder<TSource, TDestination>
        where TSource : class
        where TDestination : class, new()
    { }
}