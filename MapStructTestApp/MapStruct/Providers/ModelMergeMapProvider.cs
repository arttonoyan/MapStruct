﻿using System;
using System.Linq;
using System.Linq.Expressions;
using MapStruct.Builders.Helpers;
using MapStruct.Cashe;
using MapStruct.Configurations;

namespace MapStruct.Providers
{
    internal sealed class ModelMergeMapProvider : ICreationTimeMappingProvider
    {
        public LambdaExpression CreateExpression<TSource, TDestination>(ConfigurationSettings configuration)
        {
            Type model1Type = typeof(TDestination);
            Type model2Type = typeof(TSource);

            var parameter1 = Expression.Parameter(model1Type, "model1");
            var parameter2 = Expression.Parameter(model2Type, "model2");

            foreach (var member in model1Type.GetProperties().Where(pi => !configuration.memberBindings.ContainsKey(pi.Name)))
            {
                var memberExp = Expression.MakeMemberAccess(parameter1, member);
                configuration.memberBindings.Add(member.Name, Expression.Bind(member, memberExp));
            }

            NewExpression model = Expression.New(model1Type);
            MemberInitExpression memberInitExpression = Expression.MemberInit(model, configuration.memberBindings.Values);

            var exprBody = ExpressionVisitorFactory.AllParametersReplacer(parameter1, parameter2).Visit(memberInitExpression);
            return Expression.Lambda<Func<TSource, TDestination, TDestination>>(exprBody, parameter2, parameter1);
        }

        public void ExecuteAndCashe<TSource, TDestination>(LambdaExpression expression)
        {
            var lambda = expression as Expression<Func<TSource, TDestination, TDestination>>;
            if (lambda != null)
            {
                var func = lambda.Compile();
                CasheModel<TSource, TDestination>.SetMapper(func);
            }

            //TODO [Artyom Tonoyan] [08/02/2018]: add Exception.
            throw new Exception();
        }
    }
}