﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using MapStruct.Builders.Helpers;
using MapStruct.Cashe;
using MapStruct.Configurations;
using MapStruct.Helpers;

namespace MapStruct.Providers
{
    internal sealed class ModelMapProvider : ICreationTimeMappingProvider
    {
        public LambdaExpression CreateExpression<TSource, TDestination>(ConfigurationSettings configuration)
        {
            Type destType = typeof(TDestination);
            Type sourceType = typeof(TSource);

            Dictionary<string, PropertyInfo> sourcePiDic = sourceType.GetProperties().ToDictionary(pi => pi.Name, pi => pi);
            IEnumerable<PropertyInfo> destPis = destType.GetProperties().Where(pi => !configuration.memberBindings.ContainsKey(pi.Name) && !configuration.ignoreMembers.Contains(pi.Name) && sourcePiDic.ContainsKey(pi.Name));
            configuration.BindingsConfigurations();

            var parameter = Expression.Parameter(sourceType, "model");
            foreach (PropertyInfo destMember in destPis)
            {
                var memberExp = Expression.MakeMemberAccess(parameter, sourcePiDic[destMember.Name]);
                Type sourceMemberType = sourcePiDic[destMember.Name].PropertyType;
                if (destMember.PropertyType == sourceMemberType)
                {
                    configuration.memberBindings.Add(destMember.Name, Expression.Bind(destMember, memberExp));
                }
                else
                {
                    MethodInfo mi = ReflectionHelper.GetMapConvertMethodInfo(destMember.PropertyType, sourceMemberType);
                    if (mi != null)
                    {
                        var changeTypeExp = Expression.Call(mi, memberExp);
                        configuration.memberBindings.Add(destMember.Name, Expression.Bind(destMember, changeTypeExp));
                    }
                    else
                    {
                        var valueExp = Expression.Convert(memberExp, destMember.PropertyType);
                        configuration.memberBindings.Add(destMember.Name, Expression.Bind(destMember, valueExp));
                    }
                }
            }

            return LambdaExpressionHelper.CreateExpression<TSource, TDestination>(destType, parameter, configuration.memberBindings.Values, true);
        }

        //private static LambdaExpression CreateLambdaExpression<TSource, TDestination>(Type destType, ParameterExpression parameter, ConfigurationSettings configuration)
        //{
        //    NewExpression model = Expression.New(destType);
        //    MemberInitExpression memberInitExpression = Expression.MemberInit(model, configuration.memberBindings.Values);
        //    var exprBody = ExpressionVisitorFactory.AllParametersReplacer(parameter).Visit(memberInitExpression);
        //    return Expression.Lambda<Func<TSource, TDestination>>(exprBody, parameter);
        //}

        public void ExecuteAndCashe<TSource, TDestination>(LambdaExpression expression)
        {
            var lambda = expression as Expression<Func<TSource, TDestination>>;

            //TODO [Artyom Tonoyan] [08/02/2018]: add Exception.
            if (lambda == null)
                throw new Exception();

            var func = lambda.Compile();
            CasheModel<TSource, TDestination>.SetMapper(func);
        }
    }
}
