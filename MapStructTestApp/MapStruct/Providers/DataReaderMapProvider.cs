﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using MapStruct.Cashe;
using MapStruct.Extensions;
using MapStruct.Configurations;
using MapStruct.Helpers;

namespace MapStruct.Providers
{
    internal class DataReaderMapProvider
    {
        public static Expression<Func<IDataRecord, TDestination>> CreateExpression<TDestination>(IDataRecord reader, ConfigurationSettings configuration)
            where TDestination : class, new()
        {
            var bindings = GetPropertyNames<TDestination>(reader, configuration);

            Type destType = typeof(TDestination);
            IEnumerable<PropertyInfo> properties = GetProperties(destType, bindings);

            var parameter = Expression.Parameter(typeof(IDataRecord), "ireader`");

            var miIndexator = typeof(IDataRecord).GetProperties()
                .FirstOrDefault(p => p.GetIndexParameters()
                .Any(p1 => p1.ParameterType == typeof(string)))
                ?.GetMethod;

            var sourcePiDic = GetUpperCaseFieldNameTypeDictionary(reader);
            var memberBindings = new List<MemberBinding>();
            foreach (PropertyInfo member in properties)
            {
                string name = member.Name;
                if (!bindings.IsNullOrEmpty())
                {
                    if (bindings.TryGetValue(member.Name, out string memberName))
                        name = memberName;
                }

                (string FieldName, Type FieldType) sourceMember = sourcePiDic[name];
                var indexatorExp = Expression.Call(parameter, miIndexator, Expression.Constant(sourceMember.FieldName, typeof(string)));
                if (member.PropertyType == sourceMember.FieldType)
                {
                    var valueExp = Expression.Convert(indexatorExp, member.PropertyType);
                    memberBindings.Add(Expression.Bind(member, valueExp));
                }
                else
                {
                    MethodInfo mi = ReflectionHelper.GetMapConvertMethodInfo(member.PropertyType, sourceMember.FieldType);
                    if (mi != null)
                    {
                        var changeTypeExp = Expression.Call(mi, indexatorExp);
                        memberBindings.Add(Expression.Bind(member, changeTypeExp));
                    }
                    else
                    {
                        MethodInfo miConvert = ReflectionHelper.ConvertMethodInfo(member.PropertyType);
                        if (miConvert != null)
                        {
                            var changeTypeExp = Expression.Call(miConvert, indexatorExp);
                            //var valueExp = Expression.Convert(indexatorExp, member.PropertyType);
                            memberBindings.Add(Expression.Bind(member, changeTypeExp));
                        }
                    }
                }
            }

            if (!configuration.memberExpressions.IsNullOrEmpty())
            {
                foreach (var binding in configuration.memberExpressions)
                {
                    Delegate del = binding.Expression.Compile();
                    var builder = (Linq.Builders.IDapBuilder1<object>)del.DynamicInvoke(reader);
                    Func<IDataRecord, object> func = builder.ToFunc();

                    Type memberType = ReflectionHelper.GetMemberUnderlyingType(binding.Member);
                    //DynamicObjectCashe.SetMapper<TDestination>(memberType, func);

                    Expression<Func<IDataRecord, object>> expression = r => func(r);
                    var changeTypeExp = Expression.Convert(expression.Body, memberType);
                    memberBindings.Add(Expression.Bind(binding.Member, changeTypeExp));
                }

            }
            if (!configuration.memberBindings.IsNullOrEmpty())
            {
                var binding = configuration.memberBindings.First();
                var pi = destType.GetProperties().FirstOrDefault(p => p.Name == binding.Key);
                if (pi != null)
                {
                    var exp = ((MemberAssignment) binding.Value).Expression as MethodCallExpression;
                    //var expBody = exp.Object as MethodCallExpression;
                    //var aaaa = expBody.Method.Invoke(null, new object[] {reader});
                    var exp1 = Builders.Helpers.ExpressionVisitorFactory.AllParametersReplacer(parameter).Visit(exp);
                    var lambdaExp = Expression.Lambda<Func<IDataRecord, TDestination>>(exp1, parameter);
                    var func = lambdaExp.Compile();


                    //var exprBody = Builders.Helpers.ExpressionVisitorFactory.AllParametersReplacer(parameter).Visit(((MemberAssignment)binding.Value).Expression);
                    //var aaa = Expression.Lambda<Func<IDataRecord, TDestination>>(exprBody, parameter);
                    //Func<IDataRecord, TDestination> convert = aaa.Compile();
                    
                    NewExpression model = Expression.New(pi.PropertyType);
                    MemberInitExpression memberInitExpression = Expression.MemberInit(model, binding.Value);
                    var exprBody = Builders.Helpers.ExpressionVisitorFactory.AllParametersReplacer(parameter).Visit(memberInitExpression);
                    var aaa = Expression.Lambda<Func<IDataRecord, TDestination>>(exprBody, parameter);
                    Func<IDataRecord, TDestination> convert = aaa.Compile();

                    //var changeTypeExp = Expression.Call(pi, indexatorExp);
                    memberBindings.Add(Expression.Bind(pi, ((MemberAssignment)binding.Value).Expression));
                    //memberBindings.Add(binding.Value);
                }
            }

            return LambdaExpressionHelper.CreateExpression<IDataRecord, TDestination>(destType, parameter, memberBindings, true);
        }

        private static IReadOnlyDictionary<string, string> GetPropertyNames<TModel>(IDataRecord reader, ConfigurationSettings casheConfig)
        {
            var columnNamesTypesDic = GetUpperCaseFieldNameTypeDictionary(reader);
            Dictionary<string, string> columnNamesDic = casheConfig.useStandardCodeStyleForMembers ?
                columnNamesTypesDic.Keys.ToDictionary(p => p.Replace("_", ""), p => p) :
                columnNamesTypesDic.Keys.ToDictionary(p => p, p => p);

            Type modelType = typeof(TModel);
            var properties = casheConfig.ignoreMembers.IsNullOrEmpty()
                ? modelType.GetProperties()
                : modelType.GetProperties().Where(pi => !casheConfig.ignoreMembers.Contains(pi.Name));

            var piDic = properties
                .Where(pi => columnNamesDic.ContainsKey(pi.Name.ToUpper()))
                .ToDictionary(pi => pi.Name, pi => columnNamesDic[pi.Name.ToUpper()]);

            if (!casheConfig.memberNameBindings.IsNullOrEmpty())
            {
                foreach (var b in casheConfig.memberNameBindings)
                    piDic[b.Key] = b.Value;
            }

            return new ReadOnlyDictionary<string, string>(piDic);
        }

        private static IEnumerable<PropertyInfo> GetProperties(Type modelType, IReadOnlyDictionary<string, string> bindings)
        {
            bool IsNotClass(PropertyInfo pi) => !(pi.PropertyType.IsClass && pi.PropertyType.Name != typeof(string).Name);
            return bindings.IsNullOrEmpty() ?
                modelType.GetProperties().Where(IsNotClass) :
                modelType.GetProperties().Where(pi => bindings.ContainsKey(pi.Name) && IsNotClass(pi));
        }

        private static Dictionary<string, (string FieldName, Type FieldType)> GetUpperCaseFieldNameTypeDictionary(IDataRecord reader)
            => GetFieldNameTypeDictionary(reader, colName => colName.ToUpper());

        private static Dictionary<string, (string FieldName, Type FieldType)> GetFieldNameTypeDictionary(IDataRecord reader, Func<string, string> func)
        {
            var items = new Dictionary<string, (string FieldName, Type FieldType)>(reader.FieldCount);
            for (int i = 0; i < reader.FieldCount; i++)
            {
                string name = reader.GetName(i);
                Type type = reader.GetFieldType(i);
                items.Add(func(name), (name, type));
            }
            return items;
        }
    }
}

