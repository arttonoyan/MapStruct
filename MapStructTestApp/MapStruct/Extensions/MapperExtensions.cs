﻿using MapStruct.Linq;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace MapStruct.Extensions
{
    public static class MapperExtensions
    {
        //public static IEnumerable<TDestination> MapConvert<TSource, TDestination>(this IEnumerable<TSource> source)
        //    where TSource : class
        //    where TDestination : class, new()
        //{
        //    var Convert = Mapper.Converter<TSource, TDestination>();
        //    return source.Select(Convert);
        //}

        public static IMapEnumerable<TSource> MapConvert<TSource>(this IEnumerable<TSource> source)
            where TSource : class
        {
            return new MapEnumerable<TSource>(source);
        }

        public static IDictionary<TKey, TDestination> MapConvert<TSource, TDestination, TKey>(this IDictionary<TKey, TSource> source)
            where TSource : class
            where TDestination : class, new()
        {
            var Convert = Mapper.Converter<TSource, TDestination>();
            return source.ToDictionary(p => p.Key, p => Convert(p.Value));
        }

        public static IEnumerable<TModel> MapConvert<TModel>(this IDataReader reader)
            where TModel : class, new()
        {
            var ConvertToModel = Mapper.Converter<TModel>(reader);
            while (reader.Read())
                yield return ConvertToModel(reader);
        }
    }
}