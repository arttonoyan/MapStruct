﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using MapStruct.Extensions;

namespace MapStruct.Configurations
{
    internal class ConfigurationSettings : IDisposable
    {
        public ConfigurationSettings()
        {
            memberBindings = new Dictionary<string, MemberBinding>();
            memberNameBindings = new Dictionary<string, string>();
            ignoreMembers = new HashSet<string>();
            //memberExpressions = new Dictionary<MemberInfo, LambdaExpression>();
            memberExpressions = new List<(MemberInfo Member, LambdaExpression Expression)>();
        }

        internal IDictionary<string, MemberBinding> memberBindings;
        internal IDictionary<string, string> memberNameBindings;

        //internal IDictionary<MemberInfo, LambdaExpression> memberExpressions;
        //internal IList<(MemberInfo Member, LambdaExpression Expression)> memberExpressions;
        internal IList<(MemberInfo Member, LambdaExpression Expression)> memberExpressions;
        internal HashSet<string> ignoreMembers;
        internal bool useStandardCodeStyleForMembers;

        public void BindingsConfigurations()
        {
            if (!memberBindings.IsNullOrEmpty() && !ignoreMembers.IsNullOrEmpty())
                memberBindings.RemoveIfContains(ignoreMembers);
        }

        #region IDisposable Support

        private bool _disposedValue;

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    memberBindings = null;
                    memberNameBindings = null;
                    ignoreMembers = null;
                    useStandardCodeStyleForMembers = false;
                }

                _disposedValue = true;
            }
        }

        #endregion
    }
}