﻿using System;
using System.Collections.Generic;
using System.Data;

namespace MapStruct.Cashe
{
    internal static class DynamicObjectCashe
    {
        static DynamicObjectCashe()
        {
            _cashe = new /*Concurrent*/Dictionary<string, Func<IDataRecord, dynamic>>();
        }

        private static /*Concurrent*/Dictionary<string, Func<IDataRecord, dynamic>> _cashe;

        public static void SetMapper<TMainModel>(Type destinationType, Func<IDataRecord, dynamic> func)
        {
            string key = Key<TMainModel>(destinationType);
            _cashe[key] = func;
        }

        private static string Key<TModel>(Type type)
        {
            return $"{typeof(TModel).Name}_{type.Name}";
        }
    }
}
