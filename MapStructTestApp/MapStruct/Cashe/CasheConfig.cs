﻿using System;
using System.Collections.Generic;
using MapStruct.Extensions;

namespace MapStruct.Cashe
{
    internal class CasheConfig : IDisposable
    {
        internal IDictionary<string, string> bindings;
        internal HashSet<string> ignoreMembers;
        internal bool useStandardCodeStyleForMembers;

        public void BindingsConfigurations()
        {
            if (!bindings.IsNullOrEmpty() && !ignoreMembers.IsNullOrEmpty())
                bindings.RemoveIfContains(ignoreMembers);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return;

            bindings = null;
            ignoreMembers = null;
        }
    }
}