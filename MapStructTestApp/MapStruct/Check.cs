﻿using System;
using System.Diagnostics;

namespace MapStruct
{
    [DebuggerStepThrough]
    internal static class Check
    {
        public static bool TryGetUnderlyingType(Type propType, out Type underlyingType)
        {
            underlyingType = System.Nullable.GetUnderlyingType(propType);
            return underlyingType != null;
        }

        public static bool Nullable(Type propType)
        {
            return System.Nullable.GetUnderlyingType(propType) != null;
        }

        public static object DBNullValue(object obj)
        {
            return obj == DBNull.Value ? null : obj;
        }
    }
}