﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using MapStruct.Builders.Helpers;

namespace MapStruct.Helpers
{
    internal static class LambdaExpressionHelper
    {
        public static MemberInfo GetMember(this LambdaExpression expression)
        {
            if (expression.Body is MemberExpression memberExpression)
                return memberExpression.Member;

            throw new InvalidCastException();
        }

        public static string GetMemberName(this LambdaExpression expression)
        {
            return GetMember(expression).Name;
        }

        public static Expression<Func<TSource, TDestination>> CreateExpression<TSource, TDestination>(Type modelType, ParameterExpression parameter, IEnumerable<MemberBinding> memberBindings, bool visit = false)
        {
            NewExpression model = Expression.New(modelType);
            MemberInitExpression memberInitExpression = Expression.MemberInit(model, memberBindings);

            if (!visit)
                return Expression.Lambda<Func<TSource, TDestination>>(memberInitExpression, parameter);

            var exprBody = ExpressionVisitorFactory.AllParametersReplacer(parameter).Visit(memberInitExpression);
            return Expression.Lambda<Func<TSource, TDestination>>(exprBody, parameter);
        }
    }
}
