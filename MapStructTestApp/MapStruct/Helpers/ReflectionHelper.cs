﻿using System;
using System.Linq;
using System.Reflection;

namespace MapStruct.Helpers
{
    internal static class ReflectionHelper
    {
        public static MethodInfo GetMapConvertMethodInfo(Type destinationMemberType, Type sourceMemberType)
        {
            #region ToBoolean
            if (destinationMemberType == typeof(bool) && sourceMemberType.IsPrimitive)
                return typeof(MapConvert).GetMethods().FirstOrDefault(p => p.Name == $"{nameof(MapConvert.ToBoolean)}");

            if (destinationMemberType == typeof(bool) && Check.Nullable(sourceMemberType))
                return typeof(MapConvert).GetMethods().FirstOrDefault(p => p.Name == $"{nameof(MapConvert.ToBoolean)}");

            if (destinationMemberType == typeof(bool) && sourceMemberType == typeof(string))
                return typeof(MapConvert).GetMethods().FirstOrDefault(p => p.Name == $"{nameof(MapConvert.ToBoolean)}");
            #endregion

            #region AsBoolean

            if (destinationMemberType == typeof(bool?) && sourceMemberType.IsPrimitive)
                return typeof(MapConvert).GetMethods().FirstOrDefault(p => p.Name == $"{nameof(MapConvert.AsBoolean)}");

            if (destinationMemberType == typeof(bool?) && Check.Nullable(sourceMemberType))
                return typeof(MapConvert).GetMethods().FirstOrDefault(p => p.Name == $"{nameof(MapConvert.AsBoolean)}");

            if (destinationMemberType == typeof(bool?) && sourceMemberType == typeof(string))
                return typeof(MapConvert).GetMethods().FirstOrDefault(p => p.Name == $"{nameof(MapConvert.AsBoolean)}");

            #endregion

            if (destinationMemberType.IsPrimitive && Check.Nullable(sourceMemberType))
                return MapConvertMethodInfo(destinationMemberType, sourceMemberType, "To");

            if (destinationMemberType.IsPrimitive && sourceMemberType == typeof(string))
                return MapConvertMethodInfo(destinationMemberType, sourceMemberType, "To");

            if (Check.TryGetUnderlyingType(destinationMemberType, out Type underlyingType) && sourceMemberType.IsPrimitive)
                return MapConvertMethodInfo(underlyingType, sourceMemberType, "As");

            if (Check.TryGetUnderlyingType(destinationMemberType, out underlyingType) && sourceMemberType == typeof(string))
                return MapConvertMethodInfo(underlyingType, sourceMemberType, "As");

            return null;
        }

        private static MethodInfo MapConvertMethodInfo(Type destinationMemberType, Type sourceMemberType, string sufix)
        {
            return typeof(MapConvert).GetMethods().FirstOrDefault(p => p.Name == $"{sufix}{destinationMemberType.Name}" && p.GetParameters().First().ParameterType == sourceMemberType);
        }

        public static MethodInfo ConvertMethodInfo(Type destinationMemberType)
        {
            Type sourceMemberType = typeof(object);
            return typeof(Convert).GetMethods().FirstOrDefault(p => p.Name == $"To{destinationMemberType.Name}" && p.GetParameters().First().ParameterType == sourceMemberType);
        }

        public static Type GetMemberUnderlyingType(MemberInfo member)
        {
            switch (member.MemberType)
            {
                case MemberTypes.Field:
                    return ((FieldInfo)member).FieldType;
                case MemberTypes.Property:
                    return ((PropertyInfo)member).PropertyType;
                case MemberTypes.Event:
                    return ((EventInfo)member).EventHandlerType;
                default:
                    throw new ArgumentException("MemberInfo must be if type FieldInfo, PropertyInfo or EventInfo", "member");
            }
        }
    }
}