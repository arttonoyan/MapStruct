﻿using System;

namespace MapStruct
{
    public static class MapConvert
    {
        #region ToType

        public static bool ToBoolean(bool? value)
        {
            return value ?? false;
        }

        public static byte ToByte(byte? value)
        {
            return value ?? 0;
        }

        public static short ToInt16(short? value)
        {
            return value ?? 0;
        }

        public static short ToInt16(byte? value)
        {
            return value ?? 0;
        }

        public static short ToInt16(sbyte? value)
        {
            return value ?? 0;
        }

        public static int ToInt32(int? value)
        {
            return value ?? 0;
        }

        public static int ToInt32(short? value)
        {
            return value ?? 0;
        }

        public static int ToInt32(ushort? value)
        {
            return value ?? 0;
        }

        public static int ToInt32(byte? value)
        {
            return value ?? 0;
        }

        public static int ToInt32(sbyte? value)
        {
            return value ?? 0;
        }

        public static long ToInt64(long? value)
        {
            return value ?? 0;
        }

        public static long ToInt64(int? value)
        {
            return value ?? 0;
        }

        public static long ToInt64(uint? value)
        {
            return value ?? 0;
        }

        public static long ToInt64(short? value)
        {
            return value ?? 0;
        }

        public static long ToInt64(ushort? value)
        {
            return value ?? 0;
        }

        public static long ToInt64(byte? value)
        {
            return value ?? 0;
        }

        public static long ToInt64(sbyte? value)
        {
            return value ?? 0;
        }

        public static sbyte ToSByte(sbyte? value)
        {
            return value ?? 0;
        }

        public static ushort ToUInt16(ushort? value)
        {
            return value ?? 0;
        }

        public static ushort ToUInt16(byte? value)
        {
            return value ?? 0;
        }

        public static uint ToUInt32(uint? value)
        {
            return value ?? 0;
        }

        public static uint ToUInt32(ushort? value)
        {
            return value ?? 0;
        }

        public static uint ToUInt32(byte? value)
        {
            return value ?? 0;
        }

        public static ulong ToUInt64(ulong? value)
        {
            return value ?? 0;
        }

        public static ulong ToUInt64(uint? value)
        {
            return value ?? 0;
        }

        public static ulong ToUInt64(ushort? value)
        {
            return value ?? 0;
        }

        public static ulong ToUInt64(byte? value)
        {
            return value ?? 0;
        }

        public static float ToSingle(float? value)
        {
            return value ?? 0;
        }

        public static float ToSingle(int? value)
        {
            return value ?? 0;
        }

        public static float ToSingle(uint? value)
        {
            return value ?? 0;
        }

        public static float ToSingle(short? value)
        {
            return value ?? 0;
        }

        public static float ToSingle(ushort? value)
        {
            return value ?? 0;
        }

        public static float ToSingle(byte? value)
        {
            return value ?? 0;
        }

        public static float ToSingle(sbyte? value)
        {
            return value ?? 0;
        }

        public static double ToDouble(double? value)
        {
            return value ?? 0;
        }

        public static double ToDouble(decimal? value)
        {
            return value == null ? 0 : Convert.ToDouble(value);
        }

        public static double ToDouble(float? value)
        {
            return value ?? 0;
        }

        public static double ToDouble(long? value)
        {
            return value ?? 0;
        }

        public static double ToDouble(ulong? value)
        {
            return value ?? 0;
        }

        public static double ToDouble(int? value)
        {
            return value ?? 0;
        }

        public static double ToDouble(uint? value)
        {
            return value ?? 0;
        }

        public static double ToDouble(short? value)
        {
            return value ?? 0;
        }

        public static double ToDouble(ushort? value)
        {
            return value ?? 0;
        }

        public static double ToDouble(byte? value)
        {
            return value ?? 0;
        }

        public static double ToDouble(sbyte? value)
        {
            return value ?? 0;
        }

        public static decimal ToDecimal(decimal? value)
        {
            return value ?? 0;
        }

        public static decimal ToDecimal(double? value)
        {
            return value == null ? 0 : Convert.ToDecimal(value);
        }

        public static decimal ToDecimal(float? value)
        {
            return value == null ? 0 : Convert.ToDecimal(value);
        }

        public static decimal ToDecimal(long? value)
        {
            return value ?? 0;
        }

        public static decimal ToDecimal(ulong? value)
        {
            return value ?? 0;
        }

        public static decimal ToDecimal(int? value)
        {
            return value ?? 0;
        }

        public static decimal ToDecimal(uint? value)
        {
            return value ?? 0;
        }

        public static decimal ToDecimal(short? value)
        {
            return value ?? 0;
        }

        public static decimal ToDecimal(ushort? value)
        {
            return value ?? 0;
        }

        public static decimal ToDecimal(byte? value)
        {
            return value ?? 0;
        }

        public static decimal ToDecimal(sbyte? value)
        {
            return value ?? 0;
        }

        public static DateTime ToDateTime(DateTime? value)
        {
            return value ?? default(DateTime);
        }

        #endregion

        #region AsType

        public static bool AsBoolean(bool? nvalue)
        {
            return nvalue.HasValue && nvalue.Value;
        }

        public static byte AsByte(byte? value)
        {
            return AsType(value);
        }

        public static sbyte AsSByte(sbyte? value)
        {
            return AsType(value);
        }

        public static short AsInt16(short? value)
        {
            return AsType(value);
        }

        public static short AsInt16(byte? value)
        {
            return AsType(value);
        }

        public static short AsInt16(sbyte? value)
        {
            return AsType(value);
        }

        public static ushort AsUInt16(ushort? value)
        {
            return AsType(value);
        }

        public static ushort AsUInt16(byte? value)
        {
            return AsType(value);
        }

        public static int AsInt32(int? value)
        {
            return AsType(value);
        }

        public static int AsInt32(short? value)
        {
            return AsType(value);
        }

        public static int AsInt32(ushort? value)
        {
            return AsType(value);
        }

        public static int AsInt32(byte? value)
        {
            return AsType(value);
        }

        public static int AsInt32(sbyte? value)
        {
            return AsType(value);
        }

        public static uint AsUInt32(uint? value)
        {
            return AsType(value);
        }

        public static uint AsUInt32(ushort? value)
        {
            return AsType(value);
        }

        public static uint AsUInt32(byte? value)
        {
            return AsType(value);
        }

        public static long AsInt64(long? value)
        {
            return AsType(value);
        }

        public static long AsInt64(int? value)
        {
            return AsType(value);
        }

        public static long AsInt64(uint? value)
        {
            return AsType(value);
        }

        public static long AsInt64(short? value)
        {
            return AsType(value);
        }

        public static long AsInt64(ushort? value)
        {
            return AsType(value);
        }

        public static long AsInt64(byte? value)
        {
            return AsType(value);
        }

        public static long AsInt64(sbyte? value)
        {
            return AsType(value);
        }

        public static ulong AsUInt64(ulong? value)
        {
            return AsType(value);
        }

        public static ulong AsUInt64(uint? value)
        {
            return AsType(value);
        }

        public static ulong AsUInt64(ushort? value)
        {
            return AsType(value);
        }

        public static ulong AsUInt64(byte? value)
        {
            return AsType(value);
        }

        public static decimal AsDecimal(decimal? value)
        {
            return AsType(value);
        }

        public static decimal AsDecimal(double? value)
        {
            return value == null ? 0 : Convert.ToDecimal(value);
        }

        public static decimal AsDecimal(float? value)
        {
            return value == null ? 0 : Convert.ToDecimal(value);
        }

        public static decimal AsDecimal(long? value)
        {
            return AsType(value);
        }

        public static decimal AsDecimal(ulong? value)
        {
            return AsType(value);
        }

        public static decimal AsDecimal(int? value)
        {
            return AsType(value);
        }

        public static decimal AsDecimal(uint? value)
        {
            return AsType(value);
        }

        public static decimal AsDecimal(short? value)
        {
            return AsType(value);
        }

        public static decimal AsDecimal(ushort? value)
        {
            return AsType(value);
        }

        public static decimal AsDecimal(byte? value)
        {
            return AsType(value);
        }

        public static decimal AsDecimal(sbyte? value)
        {
            return AsType(value);
        }

        public static double AsDouble(double? value)
        {
            return AsType(value);
        }

        public static double AsDouble(decimal? value)
        {
            return value == null ? 0 : Convert.ToDouble(value);
        }

        public static double AsDouble(float? value)
        {
            return AsType(value);
        }

        public static double AsDouble(long? value)
        {
            return AsType(value);
        }

        public static double AsDouble(ulong? value)
        {
            return AsType(value);
        }

        public static double AsDouble(int? value)
        {
            return AsType(value);
        }

        public static double AsDouble(uint? value)
        {
            return AsType(value);
        }

        public static double AsDouble(short? value)
        {
            return AsType(value);
        }

        public static double AsDouble(ushort? value)
        {
            return AsType(value);
        }

        public static double AsDouble(byte? value)
        {
            return AsType(value);
        }

        public static double AsDouble(sbyte? value)
        {
            return AsType(value);
        }

        public static float AsSingle(float? value)
        {
            return AsType(value);
        }

        public static float AsSingle(long? value)
        {
            return AsType(value);
        }

        public static float AsSingle(ulong? value)
        {
            return AsType(value);
        }

        public static float AsSingle(int? value)
        {
            return AsType(value);
        }

        public static float AsSingle(uint? value)
        {
            return AsType(value);
        }

        public static float AsSingle(short? value)
        {
            return AsType(value);
        }

        public static float AsSingle(ushort? value)
        {
            return AsType(value);
        }

        public static float AsSingle(byte? value)
        {
            return AsType(value);
        }

        public static float AsSingle(sbyte? value)
        {
            return AsType(value);
        }

        public static DateTime AsDateTime(DateTime? value)
        {
            return AsType(value);
        }

        private static TPrimitive AsType<TPrimitive>(TPrimitive? nValue)
            where TPrimitive : struct
        {
            if (!nValue.HasValue)
                return default(TPrimitive);
            return nValue.Value;
        }

        #endregion

        #region String to Type

        public static bool ToBoolean(string value)
        {
            return value == "1";
        }

        public static byte ToByte(string value)
        {
            byte.TryParse(value, out byte typeValue);
            return typeValue;
        }

        public static short ToInt16(string value)
        {
            short.TryParse(value, out short typeValue);
            return typeValue;
        }

        public static int ToInt32(string value)
        {
            int.TryParse(value, out int typeValue);
            return typeValue;
        }

        public static long ToInt64(string value)
        {
            long.TryParse(value, out long typeValue);
            return typeValue;
        }

        public static sbyte ToSByte(string value)
        {
            sbyte.TryParse(value, out sbyte typeValue);
            return typeValue;
        }

        public static ushort ToUInt16(string value)
        {
            ushort.TryParse(value, out ushort typeValue);
            return typeValue;
        }

        public static uint ToUInt32(string value)
        {
            uint.TryParse(value, out uint typeValue);
            return typeValue;
        }

        public static ulong ToUInt64(string value)
        {
            ulong.TryParse(value, out ulong typeValue);
            return typeValue;
        }

        public static float ToSingle(string value)
        {
            float.TryParse(value, out float typeValue);
            return typeValue;
        }

        public static double ToDouble(string value)
        {
            double.TryParse(value, out double typeValue);
            return typeValue;
        }

        public static decimal ToDecimal(string value)
        {
            decimal.TryParse(value, out decimal typeValue);
            return typeValue;
        }

        #endregion

        #region String as Type

        public static bool? AsBoolean(string value)
        {
            if (value == "1")
                return true;
            if (value == "0")
                return false;
            return null;
        }

        public static byte? AsByte(string value)
        {
            if (byte.TryParse(value, out byte typeValue))
                return typeValue;
            return null;
        }

        public static short? AsInt16(string value)
        {
            if (short.TryParse(value, out short typeValue))
                return typeValue;
            return null;
        }

        public static int? AsInt32(string value)
        {
            if (int.TryParse(value, out int typeValue))
                return typeValue;
            return null;
        }

        public static long? AsInt64(string value)
        {
            if (long.TryParse(value, out long typeValue))
                return typeValue;
            return null;
        }

        public static sbyte? AsSByte(string value)
        {
            if (sbyte.TryParse(value, out sbyte typeValue))
                return typeValue;
            return null;
        }

        public static ushort? AsUInt16(string value)
        {
            if (ushort.TryParse(value, out ushort typeValue))
                return typeValue;
            return null;
        }

        public static uint? AsUInt32(string value)
        {
            if (uint.TryParse(value, out uint typeValue))
                return typeValue;
            return null;
        }

        public static ulong? AsUInt64(string value)
        {
            if (ulong.TryParse(value, out ulong typeValue))
                return typeValue;
            return null;
        }

        public static float? AsSingle(string value)
        {
            if (float.TryParse(value, out float typeValue))
                return typeValue;
            return null;
        }

        public static double? AsDouble(string value)
        {
            if (double.TryParse(value, out double typeValue))
                return typeValue;
            return null;
        }

        public static decimal? AsDecimal(string value)
        {
            if (decimal.TryParse(value, out decimal typeValue))
                return typeValue;
            return null;
        }

        #endregion

        #region Primitive ToBoolean

        public static bool ToBoolean(long value)
        {
            return value == 1;
        }

        public static bool ToBoolean(ulong value)
        {
            return value == 1;
        }

        public static bool ToBoolean(long? value)
        {
            return value != null && value.Value == 1;
        }

        public static bool ToBoolean(int value)
        {
            return value == 1;
        }

        public static bool ToBoolean(uint value)
        {
            return value == 1;
        }

        public static bool ToBoolean(int? value)
        {
            return value != null && value.Value == 1;
        }

        public static bool ToBoolean(short value)
        {
            return value == 1;
        }

        public static bool ToBoolean(ushort value)
        {
            return value == 1;
        }

        public static bool ToBoolean(short? value)
        {
            return value != null && value.Value == 1;
        }

        public static bool ToBoolean(float value)
        {
            return value == 1;
        }

        public static bool ToBoolean(float? value)
        {
            return value != null && value.Value == 1;
        }

        public static bool ToBoolean(double value)
        {
            return value == 1;
        }

        public static bool ToBoolean(double? value)
        {
            return value != null && value.Value == 1;
        }

        public static bool ToBoolean(decimal value)
        {
            return value == 1;
        }

        public static bool ToBoolean(decimal? value)
        {
            return value != null && value.Value == 1;
        }

        #endregion

        #region Primitive AsBoolean

        public static bool? AsBoolean(long value)
        {
            if (value == 1)
                return true;
            if (value == 0)
                return false;
            return null;
        }

        public static bool? AsBoolean(ulong value)
        {
            if (value == 1)
                return true;
            if (value == 0)
                return false;
            return null;
        }

        public static bool? AsBoolean(long? value)
        {
            if (value == 1)
                return true;
            if (value == 0)
                return false;
            return null;
        }

        public static bool? AsBoolean(int value)
        {
            if (value == 1)
                return true;
            if (value == 0)
                return false;
            return null;
        }

        public static bool? AsBoolean(uint value)
        {
            if (value == 1)
                return true;
            if (value == 0)
                return false;
            return null;
        }

        public static bool? AsBoolean(int? value)
        {
            if (value == 1)
                return true;
            if (value == 0)
                return false;
            return null;
        }

        public static bool? AsBoolean(short value)
        {
            if (value == 1)
                return true;
            if (value == 0)
                return false;
            return null;
        }

        public static bool? AsBoolean(ushort value)
        {
            if (value == 1)
                return true;
            if (value == 0)
                return false;
            return null;
        }

        public static bool? AsBoolean(short? value)
        {
            if (value == 1)
                return true;
            if (value == 0)
                return false;
            return null;
        }

        public static bool? AsBoolean(float value)
        {
            if (value == 1)
                return true;
            if (value == 0)
                return false;
            return null;
        }

        public static bool? AsBoolean(float? value)
        {
            if (value == 1)
                return true;
            if (value == 0)
                return false;
            return null;
        }

        public static bool? AsBoolean(double value)
        {
            if (value == 1)
                return true;
            if (value == 0)
                return false;
            return null;
        }

        public static bool? AsBoolean(double? value)
        {
            if (value == 1)
                return true;
            if (value == 0)
                return false;
            return null;
        }

        public static bool? AsBoolean(decimal value)
        {
            if (value == 1)
                return true;
            if (value == 0)
                return false;
            return null;
        }

        public static bool? AsBoolean(decimal? value)
        {
            if (value == 1)
                return true;
            if (value == 0)
                return false;
            return null;
        }

        #endregion
    }
}