﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MapableConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Func<MyClass, MyClassDto, MyClass> merge = (m, dto) =>
            {
                m.Age = dto.Age;
                return m;
            };

            Func<MyClass, MyClassDto, MyClass> mergeExp = (m, dto) => m;

            var c1 = new MyClass { Age = 15 };
            var dto1 = new MyClassDto { Age = 50 };

            MyClass c2 = merge(c1, dto1);

            if (c1.Equals(c2))
            {
                int code1 = c1.GetHashCode();
                int code2 = c2.GetHashCode();
            }
        }

        class MyClass
        {
            public int Age { get; set; }
        }

        class MyClassDto
        {
            public int Age { get; set; }
        }
    }
}
