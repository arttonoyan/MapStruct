﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MapableConsoleApp1
{
    public interface IMapperable<TSource, TDestination> : IEnumerable<MapModel<TSource, TDestination>>
        where TSource : class
        where TDestination : class, new()
    {
        Type ElementType { get; }

        Expression Expression { get; }

        IMapperProvider Provider { get; }
    }

    public interface IMapperProvider
    {
        IMapperable<TSource, TDestination> CreateMap<TSource, TDestination>(ModelConfiguration configuration)
            where TSource : class
            where TDestination : class, new();

        TResult Execute<TResult>(Expression expression);
    }

    public class ModelConfiguration
    {
        
    }

    public class MapModel<TSource, TDestination>
    {
        private TSource Source { get; }
        private TDestination Destination { get; }
    }
}