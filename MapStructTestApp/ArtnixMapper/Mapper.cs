﻿using System;
using System.Data;
using MapStruct.Builders;
using MapStruct.Builders.Implementations;
using MapStruct.Cashe;

namespace MapStruct
{
    public static class Mapper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildAction">Mapping builder</param>
        public static void MapConfiguration(Action<IModelTypeBuilder> buildAction)
        {
            var entityBuilder =  new ModelTypeBuilder();
            buildAction(entityBuilder);
            entityBuilder.ConfigurationFinish();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSource">Source type to use</typeparam>
        /// <typeparam name="TDestination">Destination type</typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        public static TDestination Convert<TSource, TDestination>(TSource model)
            where TSource : class
            where TDestination : class, new() 
            => Converter<TSource, TDestination>().Invoke(model);

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSource">Source type to use</typeparam>
        /// <typeparam name="TDestination">Destination type</typeparam>
        /// <returns></returns>
        public static Func<TSource, TDestination> Converter<TSource, TDestination>()
            where TSource : class
            where TDestination : class, new()
        {
            Func<TSource, TDestination> mapper = CasheModel<TSource, TDestination>.GetModelToModelMapper();
            if (mapper != null)
                return mapper;

            MapConfiguration(cfg => cfg.CreateMap<TSource, TDestination>());
            return CasheModel<TSource, TDestination>.GetModelToModelMapper();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSource">Source type to use</typeparam>
        /// <typeparam name="TDestination">Destination type</typeparam>
        /// <returns></returns>
        public static Func<TSource, TDestination, TDestination> Mergeer<TSource, TDestination>()
            where TSource : class
            where TDestination : class, new()
        {
            Func<TSource, TDestination, TDestination> mergeer = CasheModel<TSource, TDestination>.GetModelToModelMergeMapper();
            if (mergeer != null)
                return mergeer;

            MapConfiguration(cfg => cfg.CreateMap<TSource, TDestination>());
            return CasheModel<TSource, TDestination>.GetModelToModelMergeMapper();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel">Model type to use</typeparam>
        /// <param name="reader">IDataRecord contract</param>
        /// <returns></returns>
        public static Func<IDataRecord, TModel> Converter<TModel>(IDataRecord reader)
            where TModel : class, new() 
            => CasheDataReader<TModel>.GetOrCreateDataReaderToModelMapper(reader);
    }
}