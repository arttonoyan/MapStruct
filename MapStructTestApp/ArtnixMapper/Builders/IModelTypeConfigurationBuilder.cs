﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MapStruct.Builders
{
    public interface IModelTypeConfigurationBuilder<TSource, TDestination>
        where TSource : class
        where TDestination : class, new()
    {
        IModelTypeConfigurationBuilder<TSource, TDestination> Property<TProperty>(Expression<Func<TDestination, TProperty>> model2Property, Expression<Func<TSource, TProperty>> model1Property);
        IModelTypeConfigurationBuilder<TSource, TDestination> StaticProperty<TProperty>(Expression<Func<TDestination, TProperty>> model2Property, TProperty model1Property);
        IPropertyConfigurationBuilder<TPropertyModel, TDestination> IfIsNotNull<TPropertyModel>(Expression<Func<TSource, TPropertyModel>> modelProperty) where TPropertyModel : class;
        IModelTypeConfigurationBuilder<TSource, TDestination> Ignore(IEnumerable<string> members);
        IModelTypeConfigurationBuilder<TSource, TDestination> Ignore(params string[] members);
        IModelTypeConfigurationBuilder<TSource, TDestination> Ignore(Expression<Func<TDestination, object>> predicate);
        IModelTypeConfigurationBuilder<TSource, TDestination> Property<TProperty>(Expression<Func<TSource, TProperty>> propertyExp, string columnName);
    }
}