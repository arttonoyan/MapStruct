﻿using System.Linq.Expressions;

namespace MapStruct.Builders.Helpers
{
    internal static class ExpressionVisitorFactory
    {
        public static ExpressionVisitor AllParametersReplacer(params ParameterExpression[] parameters) 
            => new ParameterReplacer(parameters);
    }
}