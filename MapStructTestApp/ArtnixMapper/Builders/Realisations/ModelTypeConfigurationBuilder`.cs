﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MapStruct.Builders.Implementations
{
    internal class ModelTypeConfigurationBuilder<TSource, TDestination> : ConfigurationBuilder, IModelTypeConfigurationBuilder<TSource, TDestination>
        where TSource : class
        where TDestination : class, new()
    {
        public ModelTypeConfigurationBuilder(ModelTypeBuilder modelTypeBuilder)
        {
            _modelTypeBuilder = modelTypeBuilder;
        }

        // The key is model2 member name.
        protected readonly ModelTypeBuilder _modelTypeBuilder;

        public IModelTypeConfigurationBuilder<TSource, TDestination> Property<TProperty>(Expression<Func<TDestination, TProperty>> model2Property, Expression<Func<TSource, TProperty>> model1Property)
        {
            OnProperty(model2Property, model1Property);
            return this;
        }

        public IModelTypeConfigurationBuilder<TSource, TDestination> Property<TProperty>(Expression<Func<TSource, TProperty>> propertyExp, string columnName)
        {
            OnProperty(propertyExp, columnName);
            return this;
        }

        public IModelTypeConfigurationBuilder<TSource, TDestination> StaticProperty<TProperty>(Expression<Func<TDestination, TProperty>> model2Property, TProperty model1Property)
        {
            OnProperty(model2Property, model1Property);
            return this;
        }

        public IModelTypeConfigurationBuilder<TSource, TDestination> Ignore(params string[] members) 
            => Ignore((IEnumerable<string>)members);

        public IModelTypeConfigurationBuilder<TSource, TDestination> Ignore(IEnumerable<string> members)
        {
            OnIgnore(members);
            return this;
        }

        public IModelTypeConfigurationBuilder<TSource, TDestination> Ignore(Expression<Func<TDestination, object>> expression)
        {
            OnIgnore(expression);
            return this;
        }

        public IPropertyConfigurationBuilder<TPropertyModel, TDestination> IfIsNotNull<TPropertyModel>(Expression<Func<TSource, TPropertyModel>> modelProperty) 
            where TPropertyModel : class 
            => new PropertyConfigurationBuilder<TPropertyModel, TDestination>(_modelTypeBuilder);

        internal virtual void Finish()
            => _modelTypeBuilder.FinishMap<TSource, TDestination>(settings);
    }
}
