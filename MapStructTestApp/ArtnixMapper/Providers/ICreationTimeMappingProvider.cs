﻿using System.Linq.Expressions;
using MapStruct.Configurations;

namespace MapStruct.Providers
{
    //internal interface IMapable<TSource, TDestination>
    //    where TSource : class
    //    where TDestination : class, new()
    //{
    //    Type ElementType { get; }

    //    Expression Expression { get; }

    //    ICreationTimeMappingProvider Provider { get; }
    //}

    //internal class Mapable<TSource, TDestination> : IMapable<TSource, TDestination>
    //    where TSource : class
    //    where TDestination : class, new()
    //{
    //    public Type ElementType { get; internal set; }
    //    public Expression Expression { get; internal set; }
    //    public ICreationTimeMappingProvider Provider { get; }
    //}

    internal interface ICreationTimeMappingProvider
    {
        LambdaExpression CreateExpression<TSource, TDestination>(ConfigurationSettings configuration);

        void ExecuteAndCashe<TSource, TDestination>(LambdaExpression expression);
    }
}