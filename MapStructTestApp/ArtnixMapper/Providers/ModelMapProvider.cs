﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using MapStruct.Builders.Helpers;
using MapStruct.Cashe;
using MapStruct.Configurations;

namespace MapStruct.Providers
{
    internal sealed class ModelMapProvider : ICreationTimeMappingProvider
    {
        public LambdaExpression CreateExpression<TSource, TDestination>(ConfigurationSettings configuration)
        {
            Type model2Type = typeof(TDestination);
            Type model1Type = typeof(TSource);

            Dictionary<string, PropertyInfo> model1PiDic = model1Type.GetProperties().ToDictionary(pi => pi.Name, pi => pi);
            IEnumerable<PropertyInfo> model2Pis = model2Type.GetProperties().Where(pi => !configuration.memberBindings.ContainsKey(pi.Name) && !configuration.ignoreMembers.Contains(pi.Name) && model1PiDic.ContainsKey(pi.Name));
            configuration.BindingsConfigurations();

            var parameter = Expression.Parameter(model1Type, "model");
            foreach (PropertyInfo member in model2Pis)
            {
                var memberExp = Expression.MakeMemberAccess(parameter, model1PiDic[member.Name]);
                Type model1MemberType = model1PiDic[member.Name].PropertyType;
                if (member.PropertyType == model1MemberType)
                {
                    configuration.memberBindings.Add(member.Name, Expression.Bind(member, memberExp));
                }
                else
                {
                    //For Nullable Types
                    if (Check.TryGetUnderlyingType(model1MemberType, out Type underlyingType))
                    {
                        MethodInfo mi = typeof(Check).GetMethods().FirstOrDefault(p => p.Name == $"To{underlyingType.Name}");
                        if (mi != null)
                        {
                            var changeTypeExp = Expression.Call(mi, memberExp);
                            configuration.memberBindings.Add(member.Name, Expression.Bind(member, changeTypeExp));
                        }
                        else
                        {
                            throw new InvalidCastException($"Can't find the To{underlyingType} method from {typeof(Check).FullName} static class.");
                        }
                    }
                    else
                    {
                        var valueExp = Expression.Convert(memberExp, member.PropertyType);
                        configuration.memberBindings.Add(member.Name, Expression.Bind(member, valueExp));
                    }
                }
            }

            NewExpression model = Expression.New(model2Type);
            MemberInitExpression memberInitExpression = Expression.MemberInit(model, configuration.memberBindings.Values);
            var exprBody = ExpressionVisitorFactory.AllParametersReplacer(parameter).Visit(memberInitExpression);
            return Expression.Lambda<Func<TSource, TDestination>>(exprBody, parameter);
        }

        public void ExecuteAndCashe<TSource, TDestination>(LambdaExpression expression)
        {
            var lambda = expression as Expression<Func<TSource, TDestination>>;

            //TODO [Artyom Tonoyan] [08/02/2018]: add Exception.
            if (lambda == null)
                throw new Exception();

            var func = lambda.Compile();
            CasheModel<TSource, TDestination>.SetMapper(func);
        }
    }
}
