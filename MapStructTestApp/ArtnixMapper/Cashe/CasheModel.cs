﻿using System;
using System.Linq.Expressions;

namespace MapStruct.Cashe
{
    internal static class CasheModel<TSource, TDestination>
    {
        private static Func<TSource, TDestination> _funcModelsMapper;
        private static Func<TSource, TDestination, TDestination> _funcModelsMerge;

        public static void SetMapper(Func<TSource, TDestination> funcModelsMapper)
        {
            _funcModelsMapper = funcModelsMapper;
        }

        public static void SetMapper(Func<TSource, TDestination, TDestination> funcModelsMerge)
        {
            _funcModelsMerge = funcModelsMerge;
        }

        public static void SetMapper(Expression<Func<TSource, TDestination>> mapperExp)
        {
            _funcModelsMapper = mapperExp.Compile();
        }

        public static void SetMapper(Expression<Func<TSource, TDestination, TDestination>> mapperExp)
        {
            _funcModelsMerge = mapperExp.Compile();
        }
        public static Func<TSource, TDestination> GetModelToModelMapper() => _funcModelsMapper;

        public static Func<TSource, TDestination, TDestination> GetModelToModelMergeMapper() => _funcModelsMerge;
    }
}