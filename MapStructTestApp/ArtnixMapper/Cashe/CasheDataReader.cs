﻿using System;
using System.Data;
using MapStruct.Configurations;
using MapStruct.Providers;

namespace MapStruct.Cashe
{
    internal static class CasheDataReader<TModel>
        //where TModel : class, new()
    {
        private static Func<IDataRecord, TModel> _funcIDataRecordMapper;
        private static ConfigurationSettings config;

        public static void SetConfiguration(ConfigurationSettings configuration)
        {
            config = configuration;
        }

        public static Func<IDataRecord, TModel> GetOrCreateDataReaderToModelMapper(IDataRecord reader)
        {
            if (_funcIDataRecordMapper != null)
                return _funcIDataRecordMapper;

            config.BindingsConfigurations();
            _funcIDataRecordMapper = DataReaderMapProvider.CreateExpression<TModel>(reader, config).Compile();

            config.Dispose();
            config = null;

            return _funcIDataRecordMapper;
        }
    }
}