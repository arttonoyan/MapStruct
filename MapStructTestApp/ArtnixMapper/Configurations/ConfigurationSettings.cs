﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MapStruct.Extensions;

namespace MapStruct.Configurations
{
    internal class ConfigurationSettings : IDisposable
    {
        public ConfigurationSettings()
        {
            memberBindings = new Dictionary<string, MemberBinding>();
            memberNameBindings = new Dictionary<string, string>();
            ignoreMembers = new HashSet<string>();
        }

        internal IDictionary<string, MemberBinding> memberBindings;
        internal IDictionary<string, string> memberNameBindings;
        internal HashSet<string> ignoreMembers;
        internal bool useStandardCodeStyleForMembers;

        public void BindingsConfigurations()
        {
            if (!memberBindings.IsNullOrEmpty() && !ignoreMembers.IsNullOrEmpty())
                memberBindings.RemoveIfContains(ignoreMembers);
        }

        #region IDisposable Support

        private bool disposedValue;

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    memberBindings = null;
                    memberNameBindings = null;
                    ignoreMembers = null;
                    useStandardCodeStyleForMembers = false;
                }

                disposedValue = true;
            }
        }

        #endregion
    }
}