﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Metadata.Ecma335;
using MapStruct;
using MapStruct.Extensions;
using MapStruct.Linq;

namespace MapStructTestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            IEnumerable<MyClass> arr = Enumerable.Empty<MyClass>();
            IEnumerable<MyClassDto> arr1 = arr.MapConvert().To<MyClassDto>();

            //Mapper.MapConfiguration(cfg => 
            //    cfg
            //    .CreateMap<MyClass, MyClassDto>()
            //    .Property(dest => dest.MyClass1Dto, m => m.MyClass1
            //                                                        .AsMapable()
            //                                                        .With<MyClass1Dto>()
            //                                                        .Convert()));

            //Mapper.MapConfiguration(cfg => cfg.CreateDataReaderMap<>());

            var dunc1 = Mapable.AsMapable<MyClass>().With<MyClassDto>().ToFunc();

            Mapper.MapConfiguration(cfg =>
            {
                cfg.CreateMap<MyClass, MyClassDto>()
                    .Property(vm => vm.MyClass1Dto, m => m
                        .AsMapable()
                        .With<MyClass1Dto>()
                        .Convert());
            });

            MyClass my = new MyClass {Age = 50, TestAge = 1500};

            MyClassDto myDto1 = my
                .AsMapable()
                .With<MyClassDto>()
                .Where(cfg => cfg
                    .Property(dto => dto.Age, m => m.Age ?? default(int))
                    .Property(dto => dto.MyClass1Dto, m => m
                        .AsMapable()
                        .With<MyClass1Dto>()
                        .Convert())
                        )
                .Convert();

            var aaa = Mapper.Convert<MyClass, MyClassDto>(my);


            Func<MyClass, MyClassDto> conv = my
                .AsMapable()
                .With<MyClassDto>()
                .Where(cfg => cfg
                    .Property(dto => dto.Age, m => m.Age ?? default(int))
                    .Ignore(dto => dto.MyClass1Dto))
                .ToFunc();

            MyClassDto myDto = my
                .AsMapable()
                .With<MyClassDto>()
                .Where(cfg => cfg
                    .Property(dto => dto.Age, m => m.Age ?? default(int))
                    .Ignore(dto => dto.MyClass1Dto))
                .Convert();

            Enumerable.Empty<MyClass>().AsMapable();
            //my.AsMapable().Convert<MyClassDto>();

            MyClassDto mdto = conv(my);

            //long? a = null;
            //bool? nb1 = Convert.ToBoolean(a);
            //a = -100;
            //bool? nb2 = Convert.ToBoolean(a);
            //a = 1;
            //bool? nb3 = Convert.ToBoolean(a);
            //a = 0;
            //bool? nb4 = Convert.ToBoolean(a);

            //a = long.MaxValue;
            //int nb5 = Convert.ToInt32(a);

            //Func<MyClass, MyClassDto, MyClass> merge1 = (m, dto) =>
            //{
            //    m.Age = dto.Age;
            //    return m;
            //};

            //Expression<Func<int?, int>> f1 = value => value ?? default(int);

            Mapper.MapConfiguration(cfg => cfg.CreateMap<MyClass, MyClassDto>());
            //Mapper.MapConfiguration(cfg => cfg.CreateMergeMap<MyClass, MyClassDto>());

            //var c1 = new MyClass { Age = 15 };
            //var dto1 = new MyClassDto { Age = 50 };

            //var merge = Mapper.Mergeer<MyClass, MyClassDto>();
            //var c2 = merge.Invoke(c1, dto1);

            //if (c1.Equals(c2))
            //{

            //}

            var c3 = new MyClass { Age = 10 };
            var converter = Mapper.Converter<MyClass, MyClassDto>();
            MyClassDto t = converter.Invoke(c3);
        }

        private static TPrimitive OnAsType<TPrimitive>(TPrimitive? obj)
            where TPrimitive : struct
        {
            if (!obj.HasValue)
                return default(TPrimitive);
            return obj.Value;
        }

        static bool CanCast(PropertyInfo sourcePi, PropertyInfo destPi)
        {
            if (sourcePi.PropertyType.IsPrimitive)
            {
                object defaultValue = DefaultValue(sourcePi.PropertyType);
                return CanChangeType(defaultValue, destPi.PropertyType);
            }

            return false;
        }

        static object DefaultValue(Type primitiveType)
        {
            switch (primitiveType.Name)
            {
                case nameof(String):
                    return "";

                case nameof(Boolean):
                    return false;

                case nameof(DateTime):
                    return DateTime.Now;

                case nameof(TimeSpan):
                    return default(TimeSpan);
                default:
                    return 0;
            }
        }

        static bool CanChangeType(object value, Type conversionType)
        {
            try
            {
                Convert.ChangeType(value, conversionType);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }

    class MyClass
    {
        public short? Age { get; set; }
        public int TestAge { get; set; }
        //public MyClass1 MyClass1 { get; set; }
    }

    class MyClassDto
    {
        public int Age { get; set; }

        public MyClass1Dto MyClass1Dto { get; set; }
    }

    class MyClass1
    {
        public short? Age { get; set; }
    }

    class MyClass1Dto
    {
        public short? TestAge { get; set; }
    }

    class Q<T> : IQueryable<T>
    {
        public Type ElementType => throw new NotImplementedException();

        public Expression Expression => throw new NotImplementedException();

        public IQueryProvider Provider => throw new NotImplementedException();

        public IEnumerator<T> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }

    class P : IQueryProvider
    {
        public IQueryable CreateQuery(Expression expression)
        {
            throw new NotImplementedException();
        }

        public IQueryable<TElement> CreateQuery<TElement>(Expression expression)
        {
            throw new NotImplementedException();
        }

        public object Execute(Expression expression)
        {
            throw new NotImplementedException();
        }

        public TResult Execute<TResult>(Expression expression)
        {
            throw new NotImplementedException();
        }
    }
}
